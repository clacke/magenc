#lang racket

(provide url-base64-encode url-base64-decode)

(require net/base64)

(define (url-base64-encode bytes)
  (string-replace
   (string-replace
    (string-trim
     (bytes->string/latin-1 (base64-encode bytes #""))
     "="
     #:left? #f
     #:repeat? #t)
    "+" "-")
   "/" "_"))

(define (url-base64-decode str)
  (base64-decode
   (string->bytes/latin-1
    (string-replace
     (string-replace str "-" "+")
     "_" "/"))))
